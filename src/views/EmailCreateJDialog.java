package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.mail.MessagingException;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;

import emails.CorreoPersonaziladoMailer;
import models.CorreoModel;
import models.UsuarioModel;
import services.UsuarioService;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.JRadioButton;

public class EmailCreateJDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField emailTxt;
	private JTextField asuntoTxt;
	String ruta;
	private JComboBox<String> emailComboBox;
	private JEditorPane editorPane;
	DefaultComboBoxModel<String> indice;
	private JButton deleteFileBtn;
	private JPanel panel;
	

	/**
	 * Create the dialog.
	 */
	public EmailCreateJDialog() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(EmailCreateJDialog.class.getResource("/img/send-button.png")));
		setTitle("Enviar correo electr\u00F3nico");
		setBounds(100, 100, 461, 355);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Datos del Mensaje", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.PREFERRED_SIZE, 259, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(1)
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE))
		);
		
		emailComboBox = new JComboBox<String>();
		
		emailTxt = new JTextField();
		emailTxt.setEnabled(false);
		emailTxt.setColumns(10);
		
		JLabel lblAsunto = new JLabel("Asunto:");
		
		asuntoTxt = new JTextField();
		asuntoTxt.setColumns(10);

		JLabel lblVaco = new JLabel("Vac\u00EDo");
		lblVaco.setIcon(new ImageIcon(EmailCreateJDialog.class.getResource("/img/box.png")));
		lblVaco.setForeground(Color.DARK_GRAY);
		
		JLabel lblDocumentoAdjunto = new JLabel("Adjunto:");
		
		JButton btnNewButton = new JButton("Seleccionar documeto");
		btnNewButton.setIcon(new ImageIcon(EmailCreateJDialog.class.getResource("/img/search.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.addChoosableFileFilter(new FileFilter() {
				    public String getDescription() {
				        return "PDF Documents (*.pdf)";
				    }
				    public boolean accept(File f) {
				        if (f.isDirectory()) {
				            return true;
				        } else {
				            return f.getName().toLowerCase().endsWith(".pdf");
				        }
				    }
				});
				int opcion = fileChooser.showSaveDialog(panel);
				if (opcion == JFileChooser.APPROVE_OPTION){
					File archivo =fileChooser.getSelectedFile();
					//Obtenemos la ruta
					ruta = archivo.toString();
					//Agregamos la extensi�n
//					if (!ruta.endsWith(".pdf")){
//						ruta += ".pdf";
//					}	
					File path = new File (ruta);
					lblVaco.setText(path.getName());
					lblVaco.setForeground(Color.BLUE);
					lblVaco.setIcon(new ImageIcon(EmailCreateJDialog.class.getResource("/img/packaging.png")));

					deleteFileBtn.setEnabled(true);
				}
			}
		});
		
		JLabel lblDescripcin = new JLabel("Descripci\u00F3n:");
		
		JScrollPane scrollPane = new JScrollPane();
		
		lblVaco.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JRadioButton rdbtnUsuarioDelSistema = new JRadioButton("Correo de usuario del sistema:");
		rdbtnUsuarioDelSistema.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				emailComboBox.setEnabled(true);
				emailTxt.setEnabled(false);
			}
		});
		rdbtnUsuarioDelSistema.setSelected(true);
		
		JRadioButton rdbtnOtroCorreoElectrnico = new JRadioButton("Otro correo electr\u00F3nico:");
		rdbtnOtroCorreoElectrnico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				emailComboBox.setEnabled(false);
				emailTxt.setEnabled(true);
			}
		});

		ButtonGroup seleccionCorreoButtonGroup = new ButtonGroup();
		seleccionCorreoButtonGroup.add(rdbtnUsuarioDelSistema);
		seleccionCorreoButtonGroup.add(rdbtnOtroCorreoElectrnico);
		
		deleteFileBtn = new JButton("");
		deleteFileBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteFileBtn.setEnabled(false);
				lblVaco.setText("Vac�o");
				lblVaco.setIcon(new ImageIcon(EmailCreateJDialog.class.getResource("/img/box.png")));
				lblVaco.setForeground(Color.DARK_GRAY);
				ruta = null;
			}
		});
		deleteFileBtn.setEnabled(false);
		deleteFileBtn.setIcon(new ImageIcon(EmailCreateJDialog.class.getResource("/img/delete.png")));
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(rdbtnOtroCorreoElectrnico)
						.addComponent(rdbtnUsuarioDelSistema)
						.addComponent(emailTxt, GroupLayout.DEFAULT_SIZE, 403, Short.MAX_VALUE)
						.addComponent(emailComboBox, 0, 403, Short.MAX_VALUE)
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAsunto)
								.addComponent(lblDescripcin)
								.addComponent(lblDocumentoAdjunto, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
							.addGap(3)
							.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
									.addGap(1)
									.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(deleteFileBtn, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(lblVaco, GroupLayout.PREFERRED_SIZE, 148, Short.MAX_VALUE))
								.addComponent(asuntoTxt, GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
								.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE))))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(rdbtnUsuarioDelSistema, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(emailComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(rdbtnOtroCorreoElectrnico, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(emailTxt, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAsunto)
						.addComponent(asuntoTxt, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(5)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblDescripcin)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE, false)
							.addComponent(lblDocumentoAdjunto, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblVaco, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
						.addComponent(deleteFileBtn, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
		);
		
		editorPane = new JEditorPane();
		scrollPane.setViewportView(editorPane);
		panel.setLayout(gl_panel);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Cancelar");
				okButton.setIcon(new ImageIcon(EmailCreateJDialog.class.getResource("/img/cancel.png")));
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Enviar");
				cancelButton.setIcon(new ImageIcon(EmailCreateJDialog.class.getResource("/img/accept.png")));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (emailComboBox.getSelectedItem().toString().equals("")) {
							JOptionPane.showMessageDialog(
									   panel,
									   "No hay usuarios registrados");
						}
						else if (rdbtnOtroCorreoElectrnico.isSelected() && emailTxt.getText().toString().equals("")) {
							JOptionPane.showMessageDialog(
									   panel,
									   "Debe ingresar un email");
						}
						else if (asuntoTxt.getText().toString().equals("")) {
							JOptionPane.showMessageDialog(
									   panel,
									   "Debe ingresar un asunto del mensaje");
						}
						else if (editorPane.getText().equals("")) {
							JOptionPane.showMessageDialog(
									   panel,
									   "No puede enviar un mensaje vac�o, agregue una descripci�n");
						}
						else {
							String email = "";
							if (rdbtnUsuarioDelSistema.isSelected()) {
								email = emailComboBox.getSelectedItem().toString();
							}
							if (rdbtnOtroCorreoElectrnico.isSelected()) {
								email = emailTxt.getText().toString();
							}
							String subject = asuntoTxt.getText().toString();
							String text = editorPane.getText();
							String path_file = ruta;
							CorreoModel correoModel = new CorreoModel(email, subject, text, path_file);
							try {
								CorreoPersonaziladoMailer correoPersonaziladoMailer = new CorreoPersonaziladoMailer(correoModel);
								correoPersonaziladoMailer.send(correoModel.getEmail());
								JOptionPane.showMessageDialog(
										   contentPanel,
										   "�Mensaje enviado exitosamente!");
								dispose();
							} catch (MessagingException e1) {
								JOptionPane.showMessageDialog(
										   contentPanel,
										   "No se pudo enviar el mensaje");
							}
						}
						
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		inicializar();
	}

	public void inicializar(){
		try {
			ArrayList<UsuarioModel> usuarios = UsuarioService.read("");
			DefaultComboBoxModel<String> emails=new DefaultComboBoxModel<String>();
			for (UsuarioModel usuarioModel: usuarios){
				emails.addElement(usuarioModel.getEmail());
			}
			emailComboBox.setModel(emails);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
