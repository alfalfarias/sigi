package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

public class MensajeCreate extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;
	String ruta;
	private JComboBox<String> comboBox;
	private JEditorPane editorPane;
	

	/**
	 * Create the dialog.
	 */
	public MensajeCreate() {

		setTitle("Mensaje - Proveedor");
		setBounds(100, 100, 407, 350);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Datos del Mensaje", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 246, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JLabel lblProveedor = new JLabel("Usuario:");
		
		comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"mariaaguilera0830@gmail.com", "-OTRO-"}));
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
//				ProveedorDAO proveedorDAO = new ProveedorDAO();
//				PersonaDAO personaDAO = new PersonaDAO();
//				textField.setText(personaDAO.getOne(proveedorDAO.getOne(Long.parseLong(comboBox.getSelectedItem().toString())).getPersona_id()).getCorreo());
//				proveedorDAO.close();
//				personaDAO.close();
			}
		});
		
		JLabel lblCorreo = new JLabel("Correo:");
		
		textField = new JTextField();
		textField.setEnabled(false);
		textField.setColumns(10);
		
		JLabel lblAsunto = new JLabel("Asunto:");
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);

		JLabel lblVaco = new JLabel("Vac\u00EDo");
		
		JLabel lblDocumentoAdjunto = new JLabel("Adjunto:");
		
		JButton btnNewButton = new JButton("Ver Documento");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.addChoosableFileFilter(new FileFilter() {
				    public String getDescription() {
				        return "PDF Documents (*.pdf)";
				    }
				    public boolean accept(File f) {
				        if (f.isDirectory()) {
				            return true;
				        } else {
				            return f.getName().toLowerCase().endsWith(".pdf");
				        }
				    }
				});
				int opcion = fileChooser.showSaveDialog(null);
				if (opcion == JFileChooser.APPROVE_OPTION){
					File archivo =fileChooser.getSelectedFile();
					//Obtenemos la ruta
					ruta = archivo.toString();
					//Agregamos la extensi�n
					if (!ruta.endsWith(".pdf")){
						ruta += ".pdf";
					}	
					File path = new File (ruta);
					lblVaco.setText(path.getName());
					lblVaco.setForeground(Color.BLUE);
				}
			}
		});
		
		JLabel lblDescripcin = new JLabel("Descripci\u00F3n:");
		
		JScrollPane scrollPane = new JScrollPane();
		
		lblVaco.setFont(new Font("Tahoma", Font.BOLD, 11));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblProveedor)
								.addComponent(lblCorreo)
								.addComponent(lblAsunto)
								.addComponent(lblDocumentoAdjunto))
							.addGap(18)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(lblVaco))
								.addComponent(comboBox, 0, 257, Short.MAX_VALUE)
								.addComponent(textField, GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
								.addComponent(textField_1, GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblDescripcin)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblProveedor)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCorreo)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAsunto)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDocumentoAdjunto)
						.addComponent(lblVaco)
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblDescripcin)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE))
					.addContainerGap())
		);
		
		editorPane = new JEditorPane();
		scrollPane.setViewportView(editorPane);
		panel.setLayout(gl_panel);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Cancelar");

				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Enviar");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
//						CorreoController correo=new CorreoController();
//						if (correo.enviarCorreo(textField.getText().toString(), textField_1.getText().toString(), editorPane.getText(), ruta)){
						if (19999 >= Math.random()) {
							JOptionPane.showMessageDialog(
									   contentPanel,
									   "�Mensaje enviado exitosamente!");
							dispose();
						}
						else{
							JOptionPane.showMessageDialog(
									   contentPanel,
									   "No se pudo enviar el mensaje");
						}
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		inicializar();
	}

	public void inicializar(){
//		ProveedorDAO proveedorDAO =new ProveedorDAO();
//		//Se guardan los valores de la tabla
//		DefaultComboBoxModel<Object> indice=new DefaultComboBoxModel<Object>();
//		ArrayList<Proveedor> proveedores = (ArrayList<Proveedor>) proveedorDAO.getAll("");
//		for (Proveedor proveedor: proveedores){
//			indice.addElement(proveedor.getId());
//		}
//		proveedorDAO.close();
//		comboBox.setModel(indice);
	}
}
