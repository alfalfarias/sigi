package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import database.MainDataStatic;
import models.UsuarioModel;
import services.AutenticacionService;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;

public class MainJFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6071751863324390860L;
	
	private JPanel contentPane;
	private JPanel panel;
	private JMenuItem mntmConfigurarConexin;
	private JMenuItem mntmEtl;
	private JMenuItem mntmUsuarios;
	private JMenuItem mntmAjustesDeUsuario;
	private JMenuItem mntmCerrarSesin;
	private JMenuItem mntmSalir;
	private JMenu mnServidorSmtp;
	private JMenuItem mntmConfiguracinDeServidor;
	private JMenuItem mntmEnviarCorreoElectrnico;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				LookAndFeel lookAndFeel = UIManager.getLookAndFeel();
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
				} catch (Exception e) {
					try {
						UIManager.setLookAndFeel(lookAndFeel);
					} catch (UnsupportedLookAndFeelException e1) {
						JOptionPane.showMessageDialog(
								   null,
								   "Error: "+e1.getMessage().toString());
					}
				} 
				try {
					MainJFrame frame = new MainJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainJFrame() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainJFrame.class.getResource("/images/login/casco.png")));
		setTitle("SIGI - Sistema Integral de Gesti\u00F3n de Indicadores");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 836, 360);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		mntmConfigurarConexin = new JMenuItem("Configurar conexi\u00F3n");
		mntmConfigurarConexin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DBConexion dbConexion=new DBConexion();
				dbConexion.setLocationRelativeTo(contentPane);
				dbConexion.setModal(true);
				dbConexion.setVisible(true);
			}
		});
		mntmConfigurarConexin.setIcon(new ImageIcon(MainJFrame.class.getResource("/images/login/radio.png")));
		mnArchivo.add(mntmConfigurarConexin);
		
		mntmEtl = new JMenuItem("ETL - Ver estado del an\u00E1lisis");
		mntmEtl.setIcon(new ImageIcon(MainJFrame.class.getResource("/img/ETL.png")));
		mnArchivo.add(mntmEtl);
		
		mntmUsuarios = new JMenuItem("Usuarios");
		mntmUsuarios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UsuariosReadJDialog usuariosReadJDialog = new UsuariosReadJDialog();
				usuariosReadJDialog.setLocationRelativeTo(contentPane);
				usuariosReadJDialog.setModal(true);
				usuariosReadJDialog.setVisible(true);
			}
		});
		mntmUsuarios.setIcon(new ImageIcon(MainJFrame.class.getResource("/img/users.png")));
		mnArchivo.add(mntmUsuarios);
		
		mntmAjustesDeUsuario = new JMenuItem("Ajustes de sesi\u00F3n");
		mntmAjustesDeUsuario.setIcon(new ImageIcon(MainJFrame.class.getResource("/img/round-account-button-with-user-inside.png")));
		mnArchivo.add(mntmAjustesDeUsuario);
		
		mntmCerrarSesin = new JMenuItem("Cerrar sesi\u00F3n");
		mntmCerrarSesin.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {
				AutenticacionService.logout();
				setPermissions();
				
				panel.removeAll();

				LoginJPanel loginJPanel = new LoginJPanel();
				loginJPanel.button_1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {		
						String usuario = loginJPanel.usuarioTextField.getText().toString();
						String clave = String.valueOf(loginJPanel.passwordField.getPassword());

						try {
							MainDataStatic.setProcessing(true);
							if (usuario.equals("")){
								MainDataStatic.setProcessing(false);
								JOptionPane.showMessageDialog(
										   panel,
										   "Debe ingresar su usuario");
							}
							else if (clave.equals("")){
								MainDataStatic.setProcessing(false);
								JOptionPane.showMessageDialog(
										   panel,
										   "Debe ingresar una contrase�a");
							} else if (AutenticacionService.login(usuario, clave)) {
								setPermissions();
								panel.removeAll();
								HomeJPanel homeJPanel=new HomeJPanel();
								panel.add(homeJPanel, BorderLayout.CENTER);
								panel.revalidate();
								panel.repaint();
							}
							else {
								MainDataStatic.setProcessing(false);
								JOptionPane.showMessageDialog(
										panel,
										"Credenciales inv�lidas");
							}
						} catch (SQLException | HeadlessException e) {
							// TODO Auto-generated catch block
							JOptionPane.showMessageDialog(
									panel,
									"Ha fallado la conexi�n con la base de datos");
						} finally {
							MainDataStatic.setProcessing(false);
						}
					}
				});
				
				panel.add(loginJPanel, BorderLayout.CENTER);
				panel.revalidate();
				panel.repaint();
			}
		});
		mntmCerrarSesin.setIcon(new ImageIcon(MainJFrame.class.getResource("/img/close-session.png")));
		mnArchivo.add(mntmCerrarSesin);
		
		mntmSalir = new JMenuItem("Salir");
		mntmSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int confirmado = JOptionPane.showConfirmDialog(
						   panel,
						   "�Seguro que desea salir de SIGI?");
				if (JOptionPane.OK_OPTION == confirmado){
					System.exit(0);
				}
			}
		});
		mntmSalir.setIcon(new ImageIcon(MainJFrame.class.getResource("/img/exit.png")));
		mnArchivo.add(mntmSalir);
		
		mnServidorSmtp = new JMenu("Servidor SMTP");
		menuBar.add(mnServidorSmtp);
		
		mntmConfiguracinDeServidor = new JMenuItem("Configuraci\u00F3n de servidor SMTP");
		mntmConfiguracinDeServidor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConfigurationSmtpJDialog configurationSmtpJDialog = new ConfigurationSmtpJDialog();
				configurationSmtpJDialog.setLocationRelativeTo(contentPane);
				configurationSmtpJDialog.setModal(true);
				configurationSmtpJDialog.setVisible(true);
			}
		});
		mntmConfiguracinDeServidor.setIcon(new ImageIcon(MainJFrame.class.getResource("/img/mail-server.png")));
		mnServidorSmtp.add(mntmConfiguracinDeServidor);
		
		mntmEnviarCorreoElectrnico = new JMenuItem("Enviar correo electr\u00F3nico");
		mntmEnviarCorreoElectrnico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EmailCreateJDialog emailCreateJDialog = new EmailCreateJDialog();
				emailCreateJDialog.setLocationRelativeTo(contentPane);
				emailCreateJDialog.setModal(true);
				emailCreateJDialog.setVisible(true);
			}
		});
		mntmEnviarCorreoElectrnico.setIcon(new ImageIcon(MainJFrame.class.getResource("/img/send-button.png")));
		mnServidorSmtp.add(mntmEnviarCorreoElectrnico);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmInformacin = new JMenuItem("Informaci\u00F3n");
		mntmInformacin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InformacionJDialog informacionJDialog=new InformacionJDialog();
				informacionJDialog.setLocationRelativeTo(contentPane);
				informacionJDialog.setModal(true);
				informacionJDialog.setVisible(true);
			}
		});
		mntmInformacin.setIcon(new ImageIcon(MainJFrame.class.getResource("/img/info.png")));
		mnAyuda.add(mntmInformacin);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		LoginJPanel loginJPanel = new LoginJPanel();
		loginJPanel.button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {		
				String usuario = loginJPanel.usuarioTextField.getText().toString();
				String clave = String.valueOf(loginJPanel.passwordField.getPassword());

				try {
					MainDataStatic.setProcessing(true);
					if (usuario.equals("")){
						MainDataStatic.setProcessing(false);
						JOptionPane.showMessageDialog(
								   panel,
								   "Debe ingresar su usuario");
					}
					else if (clave.equals("")){
						MainDataStatic.setProcessing(false);
						JOptionPane.showMessageDialog(
								   panel,
								   "Debe ingresar una contrase�a");
					} else if (AutenticacionService.login(usuario, clave)) {
						setPermissions();
						panel.removeAll();
						HomeJPanel homeJPanel=new HomeJPanel();
						panel.add(homeJPanel, BorderLayout.CENTER);
						panel.revalidate();
						panel.repaint();
					}
					else {
						MainDataStatic.setProcessing(false);
						JOptionPane.showMessageDialog(
								panel,
								"Credenciales inv�lidas");
					}
				} catch (SQLException | HeadlessException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(
							panel,
							"Ha fallado la conexi�n con la base de datos");
				} finally {
					MainDataStatic.setProcessing(false);
				}
			}
		});
		panel = loginJPanel;
		contentPane.add(panel, BorderLayout.CENTER);
		setPermissions();
	}

	public void setPermissions() {
		UsuarioModel usuarioModel = MainDataStatic.getUsuarioSesionModel();

		if (usuarioModel == null) {
			mntmConfigurarConexin.setEnabled(true);
			mntmEtl.setEnabled(false);
			mntmUsuarios.setEnabled(false);
			mntmAjustesDeUsuario.setEnabled(false);
			mntmCerrarSesin.setEnabled(false);
			mntmSalir.setEnabled(true);
			mnServidorSmtp.setEnabled(false);
			mntmConfiguracinDeServidor.setEnabled(false);
			mntmEnviarCorreoElectrnico.setEnabled(false);
		}
		else if (usuarioModel.getTipo().equals("Administrador")) {
			mntmConfigurarConexin.setEnabled(false);
			mntmEtl.setEnabled(true);
			mntmUsuarios.setEnabled(true);
			mntmAjustesDeUsuario.setEnabled(true);
			mntmCerrarSesin.setEnabled(true);
			mntmSalir.setEnabled(true);
			mnServidorSmtp.setEnabled(true);
			mntmConfiguracinDeServidor.setEnabled(true);
			mntmEnviarCorreoElectrnico.setEnabled(true);
		}
		else if (usuarioModel.getTipo().equals("Gerente general")) {
			mntmConfigurarConexin.setEnabled(false);
			mntmEtl.setEnabled(false);
			mntmUsuarios.setEnabled(false);
			mntmAjustesDeUsuario.setEnabled(true);
			mntmCerrarSesin.setEnabled(true);
			mntmSalir.setEnabled(true);
			mnServidorSmtp.setEnabled(true);
			mntmConfiguracinDeServidor.setEnabled(false);
			mntmEnviarCorreoElectrnico.setEnabled(true);
		}
		else if (usuarioModel.getTipo().equals("Gerente de proyectos")) {
			mntmConfigurarConexin.setEnabled(false);
			mntmEtl.setEnabled(false);
			mntmUsuarios.setEnabled(false);
			mntmAjustesDeUsuario.setEnabled(true);
			mntmCerrarSesin.setEnabled(true);
			mntmSalir.setEnabled(true);
			mnServidorSmtp.setEnabled(true);
			mntmConfiguracinDeServidor.setEnabled(false);
			mntmEnviarCorreoElectrnico.setEnabled(true);
		}
		else if (usuarioModel.getTipo().equals("Gerente de operaciones")) {
			mntmConfigurarConexin.setEnabled(false);
			mntmEtl.setEnabled(false);
			mntmUsuarios.setEnabled(false);
			mntmAjustesDeUsuario.setEnabled(true);
			mntmCerrarSesin.setEnabled(true);
			mntmSalir.setEnabled(true);
			mnServidorSmtp.setEnabled(true);
			mntmConfiguracinDeServidor.setEnabled(false);
			mntmEnviarCorreoElectrnico.setEnabled(true);
		}
	}
}
