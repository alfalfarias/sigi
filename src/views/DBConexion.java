package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.HeadlessException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.TitledBorder;

import database.MainDataStatic;
import database.MySQL;
import models.ConnectionDBModel;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class DBConexion extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtPort;
	private JTextField txtHost;
	private JTextField txtDatabase;
	private JTextField txtUsername;
	private JPasswordField passwordField;

	ConnectionDBModel appDBModel = MainDataStatic.appCDBModel;
	/**
	 * Create the dialog.
	 */
	public DBConexion() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(DBConexion.class.getResource("/images/login/radio.png")));
		setResizable(false);
		setTitle("Configuraci\u00F3n de Conexi\u00F3n");
		setBounds(100, 100, 450, 236);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Configuraci\u00F3n de conexi\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JLabel label = new JLabel("Host (ingrese el URL):");
		
		JLabel label_1 = new JLabel("Puerto (MySQL: 3306):");
		
		JLabel label_2 = new JLabel("Contrase\u00F1a:");
		
		JLabel label_3 = new JLabel("Nombre de Usuario:");
		
		JLabel label_4 = new JLabel("Nombre de la Base de Datos:");
		
		txtPort = new JTextField();
		txtPort.setText(appDBModel.getPort());
		txtPort.setColumns(10);
		
		txtHost = new JTextField();
		txtHost.setText(appDBModel.getHost());
		txtHost.setColumns(10);
		
		txtDatabase = new JTextField();
		txtDatabase.setText(appDBModel.getDatabase());
		txtDatabase.setColumns(10);
		
		txtUsername = new JTextField();
		txtUsername.setText(appDBModel.getUsername());
		txtUsername.setColumns(10);
		
		passwordField = new JPasswordField(appDBModel.getPassword());
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(29)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
									.addComponent(label, GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
									.addComponent(label_1))
								.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
								.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
							.addGap(22))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)))
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(txtPort, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(txtHost, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(txtDatabase, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(txtUsername, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(passwordField))
					.addContainerGap(37, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(txtHost, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(label))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtPort, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_1))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtDatabase, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_4))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtUsername, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_3))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_2)
						.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(12, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.PREFERRED_SIZE, 162, Short.MAX_VALUE)
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Cancelar");
				okButton.setIcon(new ImageIcon(DBConexion.class.getResource("/img/cancel.png")));

				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Acceder");
				cancelButton.setIcon(new ImageIcon(DBConexion.class.getResource("/img/accept.png")));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							ConnectionDBModel appCDBModel=new ConnectionDBModel(
									txtHost.getText().toString(),
									txtPort.getText().toString(),
									txtDatabase.getText().toString(),
									txtUsername.getText().toString(),
									String.valueOf(passwordField.getPassword())
									);
							if (MySQL.pingConnect(appCDBModel) == true){
								MainDataStatic.setDataBase(appCDBModel);
								JOptionPane.showMessageDialog(
										   contentPanel,
										   "�Conexi�n realizada con �xito!");
								dispose();
							}
							else{
								JOptionPane.showMessageDialog(
										   contentPanel,
										   "�ERROR DE CONEXI�N!");
							}
						} catch (HeadlessException | ClassNotFoundException | SQLException e1) {
							JOptionPane.showMessageDialog(
									   contentPanel,
									   e1.getMessage());
						}
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
