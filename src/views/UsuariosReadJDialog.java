package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import models.UsuarioModel;
import services.UsuarioService;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class UsuariosReadJDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4242384156563518422L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UsuariosReadJDialog dialog = new UsuariosReadJDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public UsuariosReadJDialog() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(UsuariosReadJDialog.class.getResource("/img/users.png")));
		setTitle("Usuarios");
		setBounds(100, 100, 630, 336);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "B\u00FAsqueda", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"C�digo", "C�dula", "Nombres", "Apellidos", "Tipo de Usuario", "Username"}));
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JButton button = new JButton("B\u00FAsqueda");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String filtro = comboBox.getSelectedItem().toString();
				String busqueda = textField.getText().toString();
				
				String where = "";
				if (filtro.equals("C�digo")) {
					where = "WHERE id LIKE '"+busqueda+"%' ORDER BY id DESC";
				}
				if (filtro.equals("C�dula")) {
					where = "where CONCAT_WS('-', nacionalidad, cedula) LIKE '"+busqueda+"%' ORDER BY CONCAT_WS('-', nacionalidad, cedula) DESC";
				}
				if (filtro.equals("Nombres")) {
					where = "WHERE nombres LIKE '"+busqueda+"%' ORDER BY nombres DESC";
				}
				if (filtro.equals("Apellidos")) {
					where = "WHERE apellidos LIKE '"+busqueda+"%' ORDER BY apellidos DESC";
				}
				if (filtro.equals("Tipo de Usuario")) {
					where = "WHERE tipo LIKE '"+busqueda+"%' ORDER BY tipo DESC";
				}
				if (filtro.equals("Username")) {
					where = "WHERE username LIKE '"+busqueda+"%' ORDER BY username DESC";
				}
				try {
					table.setModel(new DefaultTableModel(
							new Object[][] {},
							new String[] {
								"C�digo", "C�dula", "Nombres", "Apellidos", "Tipo de Usuario", "Username"
							}
						));
					ArrayList<UsuarioModel> usuariosModel = UsuarioService.read(where);
					DefaultTableModel defaultTableModel = (DefaultTableModel) table.getModel();
					for (UsuarioModel usuarioModel : usuariosModel) {
						defaultTableModel.addRow(new Object[]{usuarioModel.getId(), usuarioModel.getNacionalidad()+"-"+usuarioModel.getCedula(), usuarioModel.getNombres(), usuarioModel.getApellidos(), usuarioModel.getTipo(), usuarioModel.getUsername(),});
					}
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(
							contentPanel,
							   e1.getMessage());
				}
			}
		});
		button.setIcon(new ImageIcon(UsuariosReadJDialog.class.getResource("/img/search.png")));
		
		JButton btnMostrarTodos = new JButton("Mostrar todos");
		btnMostrarTodos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inicializar();
			}
		});
		btnMostrarTodos.setIcon(new ImageIcon(UsuariosReadJDialog.class.getResource("/img/list.png")));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textField, GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(button, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnMostrarTodos, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnMostrarTodos)
						.addComponent(button, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Lista de Usuarios", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGap(0, 430, Short.MAX_VALUE)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGap(0, 201, Short.MAX_VALUE)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		table = new JTable();
		table.setFillsViewportHeight(true);
		table.setModel(new DefaultTableModel(
			new Object[][] {
//				{"1253", "13.258.159", "Jos� Ant�nio", "Higuera Ferm�n", UsuarioModel.USUARIO_TIPOS[0], "jhiguera"}, 
//				{"1254", "13.258.159", "Javier Eduardo", "Belmonte Pinto", UsuarioModel.USUARIO_TIPOS[2], "jbelmonte"}, 
//				{"1255", "13.258.159", "Robert Andr�s", "Romero", UsuarioModel.USUARIO_TIPOS[3], "rromero"}, 
//				{"1276", "13.258.159", "Mar�a Lourdes", "Aguilera", UsuarioModel.USUARIO_TIPOS[0], "maguilera"}, 
//				{"1279", "13.258.159", "Mariana", "Rend�n", UsuarioModel.USUARIO_TIPOS[2], "mrendon"}, 
//				{"1301", "13.258.159", "Estefan�a", "Fuentes", UsuarioModel.USUARIO_TIPOS[3], "efuentes"}, 
//				{"1354", "13.258.159", "Adri�n Jos�", "Velasquez Soto", UsuarioModel.USUARIO_TIPOS[2], "avelasquez"}, 
//				{"1355", "13.258.159", "Juan Miguel", "Salazar", UsuarioModel.USUARIO_TIPOS[3], "jsalazar"}, 
//				{"1392", "13.258.159", "Ernesto Jes�s", "Prado", UsuarioModel.USUARIO_TIPOS[3], "eprado"}, 
			},
			new String[] {
					"C�digo", "C�dula", "Nombres", "Apellidos", "Tipo de Usuario", "Username"
			}
		));
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		panel_1.setLayout(gl_panel_1);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE)
				.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
					.addGap(0))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Editar");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						UsuarioEditJDialog usuarioEditJDialog = new UsuarioEditJDialog();
						usuarioEditJDialog.setLocationRelativeTo(contentPanel);
						usuarioEditJDialog.setModal(true);
						usuarioEditJDialog.setVisible(true);
						inicializar();
					}
				});
				
				JButton btnCancelar = new JButton("Cancelar");
				btnCancelar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				btnCancelar.setIcon(new ImageIcon(UsuariosReadJDialog.class.getResource("/img/cancel.png")));
				buttonPane.add(btnCancelar);
				okButton.setIcon(new ImageIcon(UsuariosReadJDialog.class.getResource("/img/edit2.png")));
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Crear nuevo");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						UsuarioCreateJDialog usuarioCreateJDialog = new UsuarioCreateJDialog();
						usuarioCreateJDialog.setLocationRelativeTo(contentPanel);
						usuarioCreateJDialog.setModal(true);
						usuarioCreateJDialog.setVisible(true);
						inicializar();
					}
				});
				cancelButton.setIcon(new ImageIcon(UsuariosReadJDialog.class.getResource("/img/plus.png")));
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		inicializar();
	}
	public void inicializar() {
		try {
			table.setModel(new DefaultTableModel(
					new Object[][] {},
					new String[] {
						"C�digo", "C�dula", "Nombres", "Apellidos", "Tipo de Usuario", "Username"
					}
				));
			ArrayList<UsuarioModel> usuariosModel = UsuarioService.read("ORDER BY id DESC");
			DefaultTableModel defaultTableModel = (DefaultTableModel) table.getModel();
			for (UsuarioModel usuarioModel : usuariosModel) {
				defaultTableModel.addRow(new Object[]{usuarioModel.getId(), usuarioModel.getNacionalidad()+"-"+usuarioModel.getCedula(), usuarioModel.getNombres(), usuarioModel.getApellidos(), usuarioModel.getTipo(), usuarioModel.getUsername(),});
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(
					contentPanel,
					   e.getMessage());
		}
	}
}
