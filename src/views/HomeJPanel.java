package views;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JToolBar;

import database.MainDataStatic;
import models.UsuarioModel;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.BoxLayout;

public class HomeJPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3013911753985498388L;
	private JPanel panel;
	private JButton btnProyectos;
	private JButton btnMateriales;
	private JButton btnMaquinarias;
	private JButton btnLicitacion;
	private JButton btnFinanzas;

	/**
	 * Create the panel.
	 */
	public HomeJPanel() {

		panel = new JPanel();
		panel.removeAll();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		{
			UsuarioModel usuarioModel = MainDataStatic.getUsuarioSesionModel();
			
			if (usuarioModel == null) {
				JPanel jPanel = new JPanel();
				panel.add(jPanel);
			}
			else if (usuarioModel.getTipo().equals("Administrador")) {
				FinanzasJPanel finanzasJPanel = new FinanzasJPanel();
				panel.add(finanzasJPanel);
			}
			else if (usuarioModel.getTipo().equals("Gerente general")) {
				FinanzasJPanel finanzasJPanel = new FinanzasJPanel();
				panel.add(finanzasJPanel);
			}
			else if (usuarioModel.getTipo().equals("Gerente de proyectos")) {
				ProyectosReadJPanel proyectosReadJPanel = new ProyectosReadJPanel();
				panel.add(proyectosReadJPanel);
			}
			else if (usuarioModel.getTipo().equals("Gerente de operaciones")) {
				FinanzasJPanel finanzasJPanel = new FinanzasJPanel();
				panel.add(finanzasJPanel);
			}
			else {
				JPanel jPanel = new JPanel();
				panel.add(jPanel);
			}
		}
		panel.revalidate();
		panel.repaint();
		
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		
		btnProyectos = new JButton("Proyectos");
		btnProyectos.setIcon(new ImageIcon(HomeJPanel.class.getResource("/img/architect.png")));
		btnProyectos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ProyectosReadJPanel proyectosReadJPanel = new ProyectosReadJPanel();
				panel.removeAll();
				panel.add(proyectosReadJPanel);
				panel.revalidate();
				panel.repaint();
			}
		});
		
		btnFinanzas = new JButton("Finanzas");
		btnFinanzas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FinanzasJPanel finanzasJPanel = new FinanzasJPanel();
				panel.removeAll();
				panel.add(finanzasJPanel);
				panel.revalidate();
				panel.repaint();
			}
		});
		btnFinanzas.setIcon(new ImageIcon(HomeJPanel.class.getResource("/img/financial.png")));
		toolBar.add(btnFinanzas);
		toolBar.add(btnProyectos);
		
		btnLicitacion = new JButton("Evaluar Licitaci\u00F3n");
		btnLicitacion.setIcon(new ImageIcon(HomeJPanel.class.getResource("/img/financial.png")));
		btnLicitacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EvaluacionProyectoJPanel evaluacionProyectoJPanel = new EvaluacionProyectoJPanel();
				panel.removeAll();
				panel.add(evaluacionProyectoJPanel);
				panel.revalidate();
				panel.repaint();
			}
		});
		
		btnMateriales = new JButton("Materiales");
		btnMateriales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MaterialesReadJPanel materialesReadJPanel = new MaterialesReadJPanel();
				panel.removeAll();
				panel.add(materialesReadJPanel);
				panel.revalidate();
				panel.repaint();
			}
		});
		btnMateriales.setIcon(new ImageIcon(HomeJPanel.class.getResource("/img/brick-wall.png")));
		toolBar.add(btnMateriales);
		
		btnMaquinarias = new JButton("Maquinarias");
		btnMaquinarias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MaquinariasReadJPanel maquinariasReadJPanel = new MaquinariasReadJPanel();
				panel.removeAll();
				panel.add(maquinariasReadJPanel);
				panel.revalidate();
				panel.repaint();
			}
		});
		btnMaquinarias.setIcon(new ImageIcon(HomeJPanel.class.getResource("/img/bulldozer.png")));
		toolBar.add(btnMaquinarias);
		toolBar.add(btnLicitacion);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 776, Short.MAX_VALUE)
						.addComponent(toolBar, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(1))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
					.addGap(1)
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
					.addGap(1))
		);
		setLayout(groupLayout);
		setPermissions();
	}
	
	public void setPermissions() {
		UsuarioModel usuarioModel = MainDataStatic.getUsuarioSesionModel();
		
		if (usuarioModel.getTipo().equals("Administrador")) {
			btnProyectos.setEnabled(true);
			btnMateriales.setEnabled(true);
			btnMaquinarias.setEnabled(true);
			btnLicitacion.setEnabled(true);
			btnFinanzas.setEnabled(true);
		}
		if (usuarioModel.getTipo().equals("Gerente general")) {
			btnProyectos.setEnabled(true);
			btnMateriales.setEnabled(true);
			btnMaquinarias.setEnabled(true);
			btnLicitacion.setEnabled(true);
			btnFinanzas.setEnabled(true);
		}
		if (usuarioModel.getTipo().equals("Gerente de proyectos")) {
			btnProyectos.setEnabled(true);
			btnMaquinarias.setEnabled(true);
			btnMateriales.setEnabled(true);
			btnLicitacion.setEnabled(false);
			btnFinanzas.setEnabled(false);
		}
		if (usuarioModel.getTipo().equals("Gerente de operaciones")) {
			btnProyectos.setEnabled(false);
			btnMateriales.setEnabled(false);
			btnMaquinarias.setEnabled(false);
			btnLicitacion.setEnabled(false);
			btnFinanzas.setEnabled(true);
		}
	}
}
