package views.charts;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class MaquinariaJDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1267205198894610206L;
	private final JPanel contentPanel = new JPanel();
	MaquinariaJFXPanel maquinariaJFXPanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MaquinariaJDialog dialog = new MaquinariaJDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public MaquinariaJDialog() {
		setResizable(true);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
		maquinariaJFXPanel = new MaquinariaJFXPanel();
		contentPanel.add(maquinariaJFXPanel);
		setIconImage(Toolkit.getDefaultToolkit().getImage(EstadoFinanzaJDialog.class.getResource("/img/pie-chart-economy.png")));
		setTitle("Gr\u00E1fica de estado de maquinaria");
		setBounds(100, 100, 551, 500);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setIcon(new ImageIcon(EstadoFinanzaJDialog.class.getResource("/img/exit-to-app-button.png")));
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				{
					JButton btnImprimirReporteDe = new JButton("Generar reporte de estado de maquinaria");
					btnImprimirReporteDe.setIcon(new ImageIcon(EstadoFinanzaJDialog.class.getResource("/img/analytics.png")));
					buttonPane.add(btnImprimirReporteDe);
				}
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}

}