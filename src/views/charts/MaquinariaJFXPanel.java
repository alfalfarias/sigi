package views.charts;

import java.util.Arrays;

import javafx.collections.FXCollections;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class MaquinariaJFXPanel extends JFXPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5866972227180700765L;
	
	BarChart<String, Number> barChart;
	
	@SuppressWarnings("unchecked")
	public MaquinariaJFXPanel() {    
        
      //Defining the x axis               
        CategoryAxis xAxis = new CategoryAxis();   
                
        xAxis.setCategories(FXCollections.<String>observableArrayList(Arrays.asList(
           "Enero", "Febrero", "Marzo", "Abril", "Mayo"))); 
        xAxis.setLabel("Rango de fecha seleccionado");  

        //Defining the y axis 
        NumberAxis yAxis = new NumberAxis(); 
        yAxis.setLabel("Cantidad de d�as");
        
        
      //Creating the Bar chart 
        barChart = new BarChart<>(xAxis, yAxis);  
        barChart.setTitle("Grafico de Barras de estado de maquinaria"); 
        
        
      //Prepare XYChart.Series objects by setting data        
        XYChart.Series<String, Number> series1 = new XYChart.Series<>(); 
        series1.setName("Disponible"); 
        series1.getData().add(new XYChart.Data<>("Enero", 1.0)); 
        series1.getData().add(new XYChart.Data<>("Febrero", 3.0)); 
        series1.getData().add(new XYChart.Data<>("Marzo", 5.0)); 
        series1.getData().add(new XYChart.Data<>("Abril", 5.0));   
        series1.getData().add(new XYChart.Data<>("Mayo", 2.0));

        XYChart.Series<String, Number> series2 = new XYChart.Series<>(); 
        series2.setName("En uso"); 
        series2.getData().add(new XYChart.Data<>("Enero", 5.0)); 
        series2.getData().add(new XYChart.Data<>("Febrero", 6.0));
        series2.getData().add(new XYChart.Data<>("Marzo", 10.0)); 
        series2.getData().add(new XYChart.Data<>("Abril", 4.0));  
        series2.getData().add(new XYChart.Data<>("Mayo", 1.0));

        XYChart.Series<String, Number> series3 = new XYChart.Series<>(); 
        series3.setName("Mantenimiento"); 
        series3.getData().add(new XYChart.Data<>("Enero", 4.0)); 
        series3.getData().add(new XYChart.Data<>("Febrero", 2.0)); 
        series3.getData().add(new XYChart.Data<>("Marzo", 3.0)); 
        series3.getData().add(new XYChart.Data<>("Abril", 6.0));
        series3.getData().add(new XYChart.Data<>("Mayo", 3.0));
        
        XYChart.Series<String, Number> series4 = new XYChart.Series<>(); 
        series4.setName("Cantidad requerida"); 
        series4.getData().add(new XYChart.Data<>("Enero", 4.0)); 
        series4.getData().add(new XYChart.Data<>("Febrero", 2.0)); 
        series4.getData().add(new XYChart.Data<>("Marzo", 3.0)); 
        series4.getData().add(new XYChart.Data<>("Abril", 9.0));
        series4.getData().add(new XYChart.Data<>("Mayo", 7.0));

        //Setting the data to bar chart       
        barChart.getData().addAll(series1, series2, series3, series4);

        Group  root  =  new  Group(barChart);
        Scene  scene  =  new  Scene(root);
        
//        root.getChildren().add(barChart);

        Text  text  =  new  Text();
        
        text.setX(80);
        text.setY(37);
        text.setLayoutX(10);
        text.setFont(new Font(13));
        text.setText("Desde el 1 de enero de 2019, hasta el 7 de abril de 2019.");
        text.setTextAlignment(TextAlignment.CENTER);

        root.getChildren().add(text);
        
        this.setScene(scene);
	}
}

