package views.charts;

import javafx.embed.swing.JFXPanel;
import javafx.event.EventHandler;
import javafx.geometry.Side;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.*;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

public class EstadoFinanzaJFXPanel extends JFXPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5866972227180700765L;
	
	final PieChart pieChart;
	
	public EstadoFinanzaJFXPanel() {
        Group  root  =  new  Group();
        Scene  scene  =  new  Scene(root, Color.ALICEBLUE);
        
        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                new PieChart.Data("Ingresos", 13),
                new PieChart.Data("Egresos", 25),
                new PieChart.Data("Ganancia", 10),
                new PieChart.Data("Perdida", 22)
        );
//        pieChartData.get(0).getNode().setStyle(value);
//        pieChartData.get(1).getNode().setStyle("-fx-background-color: " + "slateblue" + ";");
//        pieChartData.get(2).getNode().setStyle("-fx-background-color: " + "#ffd700" + ";");
//        pieChartData.get(3).getNode().setStyle("-fx-background-color: " + "#ffd700" + ";");
        
        pieChart = new PieChart(pieChartData);
        pieChart.setTitle("Grafico de Pie del estado de finanzas");
        pieChart.setLabelLineLength(10);
        pieChart.setLegendSide(Side.BOTTOM);
        pieChart.setMaxHeight(250);
        
        final Label caption = new Label("");
        caption.setTextFill(Color.DARKORANGE);
        caption.setStyle("-fx-font: 24 arial;");

        for (final PieChart.Data data : pieChart.getData()) {
            data.getNode().addEventHandler(MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent e) {
                        caption.setTranslateX(e.getSceneX());
                        caption.setTranslateY(e.getSceneY());
                        caption.setText(String.valueOf(data.getPieValue()) + "%");
                     }
                });
        }
        
        root.getChildren().add(pieChart);

        Text  text  =  new  Text();
        
        text.setX(60);
        text.setY(40);
        text.setLayoutX(10);
        text.setFont(new Font(13));
        text.setText("Desde el 12 de marzo de 2019, hasta el 13 de diciembre de 2020.");
        text.setTextAlignment(TextAlignment.CENTER);

//        root.getChildren().add(text);
        
        this.setScene(scene);
	}
}
