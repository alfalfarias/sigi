package views.charts;

import java.util.Arrays;

import javafx.collections.FXCollections;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class ProyectoJFXPanel extends JFXPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5866972227180700765L;
	
	BarChart<String, Number> barChart;
	
	@SuppressWarnings("unchecked")
	public ProyectoJFXPanel() {    
        
      //Defining the x axis               
        CategoryAxis xAxis = new CategoryAxis();   
                
        xAxis.setCategories(FXCollections.<String>observableArrayList(Arrays.asList(
           "Marzo", "Abril", "Mayo", "Junio"))); 
        xAxis.setLabel("Rango de fecha seleccionado");  

        //Defining the y axis 
        NumberAxis yAxis = new NumberAxis(); 
        yAxis.setLabel("Cantidad de unidades");
        
        
      //Creating the Bar chart 
        barChart = new BarChart<>(xAxis, yAxis);  
        barChart.setTitle("Grafico de Barras de proyectos"); 
        
        
      //Prepare XYChart.Series objects by setting data        
        XYChart.Series<String, Number> series1 = new XYChart.Series<>(); 
        series1.setName("En ejecución"); 
        series1.getData().add(new XYChart.Data<>("Marzo", 784.0)); 
        series1.getData().add(new XYChart.Data<>("Abril", 835.0)); 
        series1.getData().add(new XYChart.Data<>("Mayo", 330.0)); 
        series1.getData().add(new XYChart.Data<>("Junio", 540.0));   

        XYChart.Series<String, Number> series2 = new XYChart.Series<>(); 
        series2.setName("Detenido"); 
        series2.getData().add(new XYChart.Data<>("Marzo", 420.0)); 
        series2.getData().add(new XYChart.Data<>("Abril", 386.0));
        series2.getData().add(new XYChart.Data<>("Mayo", 700.0)); 
        series2.getData().add(new XYChart.Data<>("Junio", 443.0));  

        XYChart.Series<String, Number> series3 = new XYChart.Series<>(); 
        series3.setName("Finalizado"); 
        series3.getData().add(new XYChart.Data<>("Marzo", 423.0)); 
        series3.getData().add(new XYChart.Data<>("Abril", 902.0)); 
        series3.getData().add(new XYChart.Data<>("Mayo", 398.0)); 
        series3.getData().add(new XYChart.Data<>("Junio", 698.0));

        //Setting the data to bar chart       
        barChart.getData().addAll(series1, series2, series3);

        Group  root  =  new  Group(barChart);
        Scene  scene  =  new  Scene(root);
        
//        root.getChildren().add(barChart);

        Text  text  =  new  Text();
        
        text.setX(80);
        text.setY(37);
        text.setLayoutX(10);
        text.setFont(new Font(13));
        text.setText("Desde el 20 de Marzo de 2019, hasta el 20 de junio de 2019.");
        text.setTextAlignment(TextAlignment.CENTER);

        root.getChildren().add(text);
        
        this.setScene(scene);
	}
}

