package views;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.border.TitledBorder;

import database.MainDataStatic;
import models.UsuarioModel;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ImageIcon;

public class FichaDeUsuario extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9161434011313338231L;

	/**
	 * Create the panel.
	 */
	public FichaDeUsuario() {

		UsuarioModel usuarioModel = MainDataStatic.getUsuarioSesionModel();
		
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, usuarioModel.getTipo(), TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("\u00DAltima conexi\u00F3n:");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(FichaDeUsuario.class.getResource("/img/obrero.png")));
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel label_2 = new JLabel("Usuario:");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel label_3 = new JLabel("Nombres:");
		label_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel label_4 = new JLabel(usuarioModel.getNombres());
		
		JLabel label_5 = new JLabel(usuarioModel.getUsername());
		
		JLabel label_6 = new JLabel("Correo:");
		label_6.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel label_7 = new JLabel("Apellidos:");
		label_7.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel label_8 = new JLabel(usuarioModel.getEmail());
		
		JLabel label_9 = new JLabel(usuarioModel.getApellidos());
		
		JLabel label_10 = new JLabel("Datos de sesi\u00F3n");
		label_10.setHorizontalAlignment(SwingConstants.CENTER);
		label_10.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		JLabel label_11 = new JLabel("Viernes 12 de agosto de 2019, a las 13:20.");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGap(0, 355, Short.MAX_VALUE)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(label, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(label_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
								.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
								.addComponent(label_3, Alignment.TRAILING))
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createSequentialGroup()
									.addGap(5)
									.addComponent(label_4, GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE))
								.addGroup(gl_panel.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(label_5, GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE))))
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(label_6, Alignment.LEADING, 0, 0, Short.MAX_VALUE)
								.addComponent(label_7, Alignment.LEADING))
							.addGap(5)
							.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addComponent(label_8, GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
								.addComponent(label_9, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)))
						.addComponent(label_10, GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
						.addComponent(label_11, GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE))
					.addGap(1))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 171, Short.MAX_VALUE)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(label_1, Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(label_10, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(label_2)
								.addComponent(label_5))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(label_3)
								.addComponent(label_4))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(label_7)
								.addComponent(label_9))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(label_6)
								.addComponent(label_8))
							.addGap(20)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_11)
						.addComponent(label)))
		);
		panel.setLayout(gl_panel);

	}

}
