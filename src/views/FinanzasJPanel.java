package views;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.border.TitledBorder;

import views.charts.EstadoFinanzaJFXPanel;

import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.UIManager;

public class FinanzasJPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3070965135442039978L;

	public FinanzasJPanel() {
		
		JPanel panel = new JPanel();
		

		EstadoFinanzaJFXPanel panel_1 = new EstadoFinanzaJFXPanel();
		
//		JPanel panel_1 = new JPanel();

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Configuraci\u00F3n de la aplicaci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Estado de finanzas hasta la fecha", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		JLabel lblDesdeLaFecha = new JLabel("Desde la fecha de inicio se ha percibido un total de");
		lblDesdeLaFecha.setHorizontalAlignment(SwingConstants.CENTER);
		lblDesdeLaFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel label = new JLabel("14.231.434 Bs.");
		label.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JLabel lblDeCostoDe = new JLabel("Inversi\u00F3n en materiales:");
		lblDeCostoDe.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		JLabel label_1 = new JLabel("14.231.434 Bs.");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JLabel lblConUnPresupuesto = new JLabel("Presupuesto:");
		lblConUnPresupuesto.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		JLabel label_2 = new JLabel("14.231.434 Bs.");
		label_2.setForeground(new Color(0, 128, 0));
		label_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);
		
		JLabel lblEnGanancias = new JLabel("en ganancias.");
		lblEnGanancias.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GroupLayout gl_panel_4 = new GroupLayout(panel_4);
		gl_panel_4.setHorizontalGroup(
			gl_panel_4.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_4.createSequentialGroup()
					.addGap(1)
					.addComponent(lblConUnPresupuesto)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(label_1, GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
					.addGap(18)
					.addComponent(lblDeCostoDe)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(label, GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
					.addContainerGap())
				.addComponent(lblDesdeLaFecha, GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
				.addGroup(gl_panel_4.createSequentialGroup()
					.addComponent(label_2, GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEnGanancias, GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE))
		);
		gl_panel_4.setVerticalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_4.createSequentialGroup()
					.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_4.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblConUnPresupuesto)
							.addComponent(label_1))
						.addGroup(gl_panel_4.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblDeCostoDe)
							.addComponent(label)))
					.addGap(6)
					.addComponent(lblDesdeLaFecha)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_4.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblEnGanancias))
					.addContainerGap())
		);
		panel_4.setLayout(gl_panel_4);
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_2, Alignment.TRAILING, 0, 0, Short.MAX_VALUE)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(panel_4, GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE)))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 42, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
							.addGap(1))
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE))
					.addGap(1))
		);
		panel.setLayout(new BorderLayout(0, 0));
		
		FichaDeUsuario panel_3 = new FichaDeUsuario();
		panel.add(panel_3, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JLabel lblFechaDeInicio = new JLabel("Fecha de inicio:");
		lblFechaDeInicio.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel lblDeJulio = new JLabel("Lunes 17 de julio de 2019");
		lblDeJulio.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblHastaAhora = new JLabel("hasta ahora.");
		lblHastaAhora.setFont(new Font("Tahoma", Font.ITALIC, 11));
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addComponent(lblFechaDeInicio)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDeJulio, GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblHastaAhora, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblFechaDeInicio, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblDeJulio, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblHastaAhora, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(10))
		);
		panel_2.setLayout(gl_panel_2);
		setLayout(groupLayout);
	}
}
