package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InformacionJDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5103654006089911922L;
	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			InformacionJDialog dialog = new InformacionJDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public InformacionJDialog() {
		setTitle("Informaci\u00F3n");
		setIconImage(Toolkit.getDefaultToolkit().getImage(InformacionJDialog.class.getResource("/img/info.png")));
		setBounds(100, 100, 380, 435);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Licencia", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		JLabel lblGrupoDeServicio = new JLabel("Programa de Maestr\u00EDa en Inform\u00E1tica Gerencial");
		lblGrupoDeServicio.setHorizontalAlignment(SwingConstants.CENTER);
		lblGrupoDeServicio.setFont(new Font("Tahoma", Font.BOLD, 14));
		JLabel lblCiudadResidencialBello = new JLabel("Sistema de Informaci\u00F3n Ejecutivo presentado como requisito");
		lblCiudadResidencialBello.setHorizontalAlignment(SwingConstants.CENTER);
		lblCiudadResidencialBello.setFont(new Font("Tahoma", Font.PLAIN, 12));
		JLabel lblComoRequisitoPara = new JLabel("Ing. Mar\u00EDa de Lourdes Aguilera P\u00E9rez");
		lblComoRequisitoPara.setHorizontalAlignment(SwingConstants.CENTER);
		lblComoRequisitoPara.setFont(new Font("Tahoma", Font.BOLD, 13));
		JLabel lblContaduraPblica = new JLabel("C.I.: 16.670.565");
		lblContaduraPblica.setHorizontalAlignment(SwingConstants.CENTER);
		lblContaduraPblica.setFont(new Font("Tahoma", Font.BOLD, 11));
		JLabel label = new JLabel("");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setIcon(new ImageIcon(InformacionJDialog.class.getResource("/img/information/udo-logo.png")));
		
		JLabel lblEscuelaDeCiencias = new JLabel("Universidad de Oriente - N\u00FAcleo Anzo\u00E1tegui");
		lblEscuelaDeCiencias.setHorizontalAlignment(SwingConstants.CENTER);
		lblEscuelaDeCiencias.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		JLabel lblParaOptarAl = new JLabel("para optar al t\u00EDtulo de Magister Scientiarum.");
		lblParaOptarAl.setHorizontalAlignment(SwingConstants.CENTER);
		lblParaOptarAl.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(lblGrupoDeServicio, GroupLayout.DEFAULT_SIZE, 545, Short.MAX_VALUE)
				.addGroup(Alignment.TRAILING, gl_contentPanel.createSequentialGroup()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblParaOptarAl, GroupLayout.DEFAULT_SIZE, 544, Short.MAX_VALUE)
						.addGroup(Alignment.LEADING, gl_contentPanel.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblComoRequisitoPara, GroupLayout.DEFAULT_SIZE, 534, Short.MAX_VALUE)
								.addComponent(lblContaduraPblica, GroupLayout.DEFAULT_SIZE, 534, Short.MAX_VALUE)))
						.addComponent(lblCiudadResidencialBello, GroupLayout.DEFAULT_SIZE, 544, Short.MAX_VALUE))
					.addGap(1))
				.addComponent(lblEscuelaDeCiencias, GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
				.addComponent(label, GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addComponent(lblEscuelaDeCiencias)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(label, GroupLayout.PREFERRED_SIZE, 208, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblGrupoDeServicio, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCiudadResidencialBello, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblParaOptarAl, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblComoRequisitoPara, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblContaduraPblica)
					.addContainerGap())
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Aceptar");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setIcon(new ImageIcon(InformacionJDialog.class.getResource("/img/accept.png")));
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
