package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.TitledBorder;

import models.UsuarioModel;
import services.UsuarioService;

//import database.PersonaDAO;
//import database.UsuarioDAO;
//import models.Persona;
//import models.Usuario;

import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JPasswordField;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class UsuarioEditJDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtCedula;
	private JTextField txtNombres;
	private JTextField txtApellidos;
	private JPasswordField passwordField;
	private JTextField txtUsername;
	private JComboBox<Long> comboBoxId;
	private JComboBox<String> comboBoxDocumento;
	private JComboBox<String> comboBoxTipo;
	private JTextField txtEmail;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UsuarioEditJDialog dialog = new UsuarioEditJDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public UsuarioEditJDialog() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(UsuarioEditJDialog.class.getResource("/img/edit2.png")));
		setResizable(false);
		setTitle("Edici\u00F3n de usuario");
		setBounds(100, 100, 381, 363);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Datos Personales", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		JLabel lblDocumento = new JLabel("C\u00E9dula");
		JLabel label_1 = new JLabel("Nombres:");
		comboBoxDocumento = new JComboBox();
		comboBoxDocumento.setModel(new DefaultComboBoxModel(UsuarioModel.USUARIO_NACIONALIDAD));
		txtCedula = new JTextField();
		txtCedula.setColumns(10);
		txtNombres = new JTextField();
		txtNombres.setColumns(10);
		JLabel label_2 = new JLabel("Apellidos:");
		txtApellidos = new JTextField();
		txtApellidos.setColumns(10);
		
		JLabel lblCorreo = new JLabel("Correo:");
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblDocumento)
						.addComponent(label_1)
						.addComponent(label_2)
						.addComponent(lblCorreo))
					.addGap(8)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(txtEmail, GroupLayout.DEFAULT_SIZE, 249, Short.MAX_VALUE)
						.addComponent(txtApellidos, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 237, Short.MAX_VALUE)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(comboBoxDocumento, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(txtCedula, GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE))
						.addComponent(txtNombres, GroupLayout.DEFAULT_SIZE, 237, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDocumento)
						.addComponent(comboBoxDocumento, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtCedula, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(txtNombres, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtApellidos, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_2))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtEmail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCorreo))
					.addContainerGap(84, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Datos de Usuario", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		passwordField = new JPasswordField();
		JLabel label_4 = new JLabel("Tipo de Usuario:");
		comboBoxTipo = new JComboBox();
		comboBoxTipo.setModel(new DefaultComboBoxModel(UsuarioModel.USUARIO_TIPOS));
		JLabel label_5 = new JLabel("Username:");
		JLabel lblContrasea = new JLabel("Contrase\u00F1a:");
		txtUsername = new JTextField();
		txtUsername.setColumns(10);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addComponent(label_4)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(comboBoxTipo, 0, 225, Short.MAX_VALUE))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(lblContrasea)
								.addComponent(label_5, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE))
							.addGap(10)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(txtUsername, GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE)
								.addComponent(passwordField, GroupLayout.DEFAULT_SIZE, 249, Short.MAX_VALUE))))
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_4)
						.addComponent(comboBoxTipo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_5))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblContrasea))
					.addContainerGap(14, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		
		JLabel lblCdigoDelUsuario = new JLabel("C\u00F3digo del Usuario:");
		
		comboBoxId = new JComboBox();
		comboBoxId.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Long id = (Long) comboBoxId.getSelectedItem();
					setUsuarioModel(id);
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(
							   contentPanel,
							   e.getMessage());
				}
			}
		});
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE)
						.addGroup(Alignment.TRAILING, gl_contentPanel.createSequentialGroup()
							.addComponent(lblCdigoDelUsuario)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(comboBoxId, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
							.addGap(96))
						.addComponent(panel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBoxId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCdigoDelUsuario))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 111, Short.MAX_VALUE)
					.addContainerGap())
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Cancelar");
				okButton.setIcon(new ImageIcon(UsuarioEditJDialog.class.getResource("/img/cancel.png")));
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Guardar cambios");
				cancelButton.setIcon(new ImageIcon(UsuarioEditJDialog.class.getResource("/img/accept.png")));

				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (txtUsername.getText().toString().equals("") || txtCedula.getText().toString().equals("") || txtNombres.getText().equals("") || txtApellidos.getText().toString().equals("") || String.valueOf(passwordField.getPassword()).equals("")){
							JOptionPane.showMessageDialog(
									   contentPanel,
									   "�No pueden haber campos vac�os!");
						}
						else{
							UsuarioModel usuarioModel = new UsuarioModel();
							usuarioModel.setId(Long.valueOf(comboBoxId.getSelectedItem().toString()));
							usuarioModel.setCedula(txtCedula.getText().toString());
							usuarioModel.setNombres(txtNombres.getText().toString());
							usuarioModel.setApellidos(txtApellidos.getText());
							usuarioModel.setUsername(txtUsername.getText());
							usuarioModel.setEmail(txtEmail.getText());
							usuarioModel.setClave(String.valueOf(passwordField.getPassword()));
							usuarioModel.setTipo(comboBoxTipo.getSelectedItem().toString());
							usuarioModel.setNacionalidad(comboBoxDocumento.getSelectedItem().toString());
							
							try {
								if (UsuarioService.find("WHERE id != '"+usuarioModel.getId()+"' AND cedula = '"+usuarioModel.getCedula()+"'") != null) {
									JOptionPane.showMessageDialog(
											   panel,
											   "�La c�dula ya ha sido registrada!");
								}
								else if (UsuarioService.find("WHERE id != '"+usuarioModel.getId()+"' AND username = '"+usuarioModel.getUsername()+"'") != null) {
									JOptionPane.showMessageDialog(
											   panel,
											   "�El nombre de usuario ya ha sido registrado!");
								}
								else if (UsuarioService.find("WHERE id != '"+usuarioModel.getId()+"' AND email = '"+usuarioModel.getEmail()+"'") != null) {
									JOptionPane.showMessageDialog(
											   panel,
											   "�El correo ya ha sido registrado!");
								}
								else {
									usuarioModel = UsuarioService.update(usuarioModel);
									JOptionPane.showMessageDialog(
											   panel,
											   "�El usuario ha sido actualizado exitosamente!");
									dispose();
								}
							} catch (SQLException e1) {
								JOptionPane.showMessageDialog(
										   panel,
										   e1.getMessage());
							}			
						}
					}
				});
				
				JButton btnEliminar = new JButton("Eliminar");
				btnEliminar.setIcon(new ImageIcon(UsuarioEditJDialog.class.getResource("/img/delete.png")));
				btnEliminar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							int confirmado = JOptionPane.showConfirmDialog(
									   contentPanel,
									   "�Seguro que desea eliminar el Usuario?");
							if (JOptionPane.OK_OPTION == confirmado){
								Long id = (Long) comboBoxId.getSelectedItem();
									UsuarioService.delete(id);
								
								JOptionPane.showMessageDialog(
										   panel,
										   "�El usuario ha sido eliminado exitosamente!");
								dispose();
							}
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(
									   panel,
									   e1.getMessage());
						}
					}
				});
				btnEliminar.setActionCommand("OK");
				buttonPane.add(btnEliminar);
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		inicializar();
	}
	public void inicializar() {
		try {
			comboBoxId.setModel(new DefaultComboBoxModel<Long>());
			
			ArrayList<UsuarioModel> usuariosModel = UsuarioService.read("");
			for (UsuarioModel usuarioModel : usuariosModel) {
				comboBoxId.addItem(usuarioModel.getId());
			}
			Long id = (Long) comboBoxId.getSelectedItem();
			setUsuarioModel(id);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(
					   contentPanel,
					   e.getMessage());
		}
	}
	public void setUsuarioModel(Long id) throws SQLException {
		UsuarioModel usuarioModel = UsuarioService.find("WHERE id = "+id);
		txtCedula.setText(usuarioModel.getCedula());
		txtNombres.setText(usuarioModel.getNombres());
		txtApellidos.setText(usuarioModel.getApellidos());
		txtUsername.setText(usuarioModel.getUsername());
		comboBoxId.setSelectedItem(usuarioModel.getId());
		comboBoxDocumento.setSelectedItem(usuarioModel.getNacionalidad());
		comboBoxTipo.setSelectedItem(usuarioModel.getTipo());
		txtEmail.setText(usuarioModel.getEmail());
	}
}
