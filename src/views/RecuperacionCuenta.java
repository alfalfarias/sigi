package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import emails.RecuperacionCuentaMailer;
import models.UsuarioModel;
import services.UsuarioService;

import javax.mail.MessagingException;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class RecuperacionCuenta extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5646817492058702940L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			RecuperacionCuenta dialog = new RecuperacionCuenta();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public RecuperacionCuenta() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(RecuperacionCuenta.class.getResource("/img/round-account-button-with-user-inside.png")));
		setTitle("Recuperaci\u00F3n de cuenta");
		setBounds(100, 100, 494, 171);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel lblIngreseSuCorreo = new JLabel("Ingrese su correo electr\u00F3nico:");
		lblIngreseSuCorreo.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngreseSuCorreo.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		textField = new JTextField();
		textField.setColumns(10);
		JLabel lblUnaNuevaContrasea = new JLabel("Una nueva contrase\u00F1a para el ingreso a su cuenta le ser\u00E1 enviada a su correo electr\u00F3nico.");
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(textField, GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
						.addComponent(lblIngreseSuCorreo, GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
						.addComponent(lblUnaNuevaContrasea))
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblIngreseSuCorreo, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblUnaNuevaContrasea)
					.addContainerGap(24, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancelar");
				cancelButton.setIcon(new ImageIcon(RecuperacionCuenta.class.getResource("/img/cancel.png")));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Enviar");
				okButton.setIcon(new ImageIcon(RecuperacionCuenta.class.getResource("/img/accept.png")));
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String email = textField.getText().toString();
						try {
				            final UsuarioModel usuarioModel = UsuarioService.find("WHERE email = '"+email+"'");
				            if (email.equals("")) {
				                JOptionPane.showMessageDialog(contentPanel, "�No pueden haber campos vac\u00edos!");
				            }
				            else if (usuarioModel == null) {
				                JOptionPane.showMessageDialog(contentPanel, "El usuario ingresado no se encuentra registrado en el sistema");
				            }
				            else if (usuarioModel != null) {
				            	RecuperacionCuentaMailer recuperacionCuentaMailer = new RecuperacionCuentaMailer(usuarioModel);
								recuperacionCuentaMailer.send(usuarioModel.getEmail());
				                JOptionPane.showMessageDialog(contentPanel, "�Contrase\u00f1a enviada con \u00e9xito, revise su bandeja de entrada!");
				                RecuperacionCuenta.this.dispose();
				            }
				        } catch (SQLException e) {
				            JOptionPane.showMessageDialog(contentPanel, "�Error al enviar la contrase\u00f1a!");
				        } catch (MessagingException e) {
				            JOptionPane.showMessageDialog(contentPanel, "No se pudo establecer la conexi�n con el servidor SMTP");
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}
}
