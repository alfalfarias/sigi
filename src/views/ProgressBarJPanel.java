package views;

import javax.swing.JPanel;
import javax.swing.JProgressBar;

import database.MainDataStatic;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

public class ProgressBarJPanel extends JPanel implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3112945275554625776L;

	JProgressBar progressBar;
	
	/**
	 * Create the panel.
	 */
	public ProgressBarJPanel() {
		progressBar = new JProgressBar();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
		);
		setLayout(groupLayout);
		Thread progressBarThread = new Thread (this, "progresBar");
		progressBarThread.start();
	}
	public void setIndeterminate(boolean isIndeterminate) {
		progressBar.setIndeterminate(isIndeterminate);
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true) {
			setIndeterminate(MainDataStatic.isProcessing());
//			System.out.println("isProcessing: "+MainDataStatic.isProcessing());
		}
	}

}
