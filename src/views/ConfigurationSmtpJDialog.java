package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.mail.MessagingException;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.TitledBorder;

import database.MainDataStatic;
import emails.TestConexionMailer;
import models.MailerModel;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class ConfigurationSmtpJDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtPort;
	private JTextField txtHost;
	private JTextField txtUsername;
	private JPasswordField passwordField;

	MailerModel mailerModel = MainDataStatic.mailerModel;
	/**
	 * Create the dialog.
	 */
	public ConfigurationSmtpJDialog() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ConfigurationSmtpJDialog.class.getResource("/img/mail-server.png")));
		setResizable(false);
		setTitle("Configuraci\u00F3n de Servidor SMTP");
		setBounds(100, 100, 450, 197);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Configuraci\u00F3n de conexi\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JLabel label = new JLabel("Host (ingrese el URL):");
		
		JLabel lblPuerto = new JLabel("Puerto:");
		
		JLabel label_2 = new JLabel("Contrase\u00F1a:");
		
		JLabel label_3 = new JLabel("Nombre de Usuario:");
		
		txtPort = new JTextField();
		txtPort.setText(mailerModel.getPort());
		txtPort.setColumns(10);
		
		txtHost = new JTextField();
		txtHost.setText(mailerModel.getHost());
		txtHost.setColumns(10);
		
		txtUsername = new JTextField();
		txtUsername.setText(mailerModel.getUsername());
		txtUsername.setColumns(10);
		
		passwordField = new JPasswordField(mailerModel.getPassword());
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
							.addComponent(label, GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
							.addComponent(lblPuerto))
						.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
					.addGap(1)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(txtPort, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(txtUsername, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(passwordField)
						.addComponent(txtHost, GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(1)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(label)
						.addComponent(txtHost, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtPort, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPuerto))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtUsername, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_3))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_2)
						.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.PREFERRED_SIZE, 162, Short.MAX_VALUE)
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Cancelar");
				okButton.setIcon(new ImageIcon(ConfigurationSmtpJDialog.class.getResource("/img/cancel.png")));

				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Acceder");
				cancelButton.setIcon(new ImageIcon(ConfigurationSmtpJDialog.class.getResource("/img/accept.png")));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							MainDataStatic.setProcessing(true);
							MailerModel mailerModel = new MailerModel(
								txtHost.getText().toString(),
								txtPort.getText().toString(),
								txtUsername.getText().toString(),
								String.valueOf(passwordField.getPassword()),
								true);
							TestConexionMailer testConexionMailer = new TestConexionMailer(mailerModel);
							testConexionMailer.send(mailerModel.getUsername());
							MainDataStatic.mailerModel = mailerModel;
							JOptionPane.showMessageDialog(
									contentPanel,
									"�Conexi�n al servidor SMTP realizada con �xito!");
							dispose();
						} catch (MessagingException e1) {
							MainDataStatic.setProcessing(false);
							JOptionPane.showMessageDialog(
									contentPanel,
									"�No se pudo hacer conexi�n con el servicio SMTP!");
						} finally {
							MainDataStatic.setProcessing(false);
						}
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
