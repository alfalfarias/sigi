DROP DATABASE sigi;
CREATE DATABASE sigi;
USE sigi;
CREATE TABLE IF NOT EXISTS usuarios (
    id INT AUTO_INCREMENT,
    nombres VARCHAR(191) NOT NULL,
    apellidos VARCHAR(191) NOT NULL,
    nacionalidad VARCHAR(191) NOT NULL,
    cedula VARCHAR(191) UNIQUE NOT NULL,
    email VARCHAR(191) UNIQUE NOT NULL,
    username VARCHAR(191) UNIQUE NOT NULL,
    clave VARCHAR(191) NOT NULL,
    tipo VARCHAR(191) NOT NULL,
    fecha_creacion DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
)  ENGINE=INNODB;
CREATE TABLE IF NOT EXISTS proyectos (
    id INT AUTO_INCREMENT,
    proyecto_id INTEGER NOT NULL,
    nombre VARCHAR(191) NOT NULL,
    estado VARCHAR(191) NOT NULL,
	periodo_en_minutos INTEGER NOT NULL,
	fecha_migracion DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS maquinarias (
    id INT AUTO_INCREMENT,
    maquinaria_id INTEGER NOT NULL,
	nombre VARCHAR(191) NOT NULL,
	estado VARCHAR(191) NOT NULL,
	periodo_en_minutos INTEGER NOT NULL,
	fecha_migracion DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS materiales (
    id INT AUTO_INCREMENT,
	material_id INTEGER NOT NULL,
	nombre VARCHAR(191) NOT NULL,
	cantidad INTEGER NOT NULL,
	periodo_en_minutos INTEGER NOT NULL,
	fecha_migracion DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
)  ENGINE=INNODB;
INSERT INTO `usuarios` (`id`, `nombres`, `apellidos`, `nacionalidad`, `cedula`, `email`, `username`, `clave`, `tipo`, `fecha_creacion`) VALUES (NULL, 'Administrador', 'Administrador', 'Administrador', 'Administrador', 'admin@example.com', 'Administrador', 'Administrador', 'Administrador', '2019-06-30 02:07:15');