package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;

import models.ConnectionDBModel;

public class MySQL {
	protected static Connection connection;
	protected static ResultSet resultSet = null;
	protected static Statement statement = null;
	protected static PreparedStatement preparedStatement = null;
	ConnectionDBModel connectionDBModel = new ConnectionDBModel();
	public boolean testConnection() {
		return true;
	}
	public boolean testConnection(ConnectionDBModel appCDBModel) {
		return true;
	}
	public static boolean pingConnect(ConnectionDBModel connectionDBModel) throws ClassNotFoundException, SQLException {
	      Class.forName("com.mysql.jdbc.Driver");
	      connection = DriverManager.getConnection("jdbc:mysql://" + connectionDBModel.getHost() + ":" + connectionDBModel.getPort() + "/" + connectionDBModel.getDatabase(), connectionDBModel.getUsername(), connectionDBModel.getPassword());
	      statement = connection.createStatement();
	      resultSet = statement.executeQuery("SELECT 1;");
	      resultSet.next();
	      statement.close();
	      connection.close();
	      return true;
	   }
}
