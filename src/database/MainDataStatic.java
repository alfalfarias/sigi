package database;

import java.io.IOException;

import models.ConnectionDBModel;
import models.MailerModel;
import models.UsuarioModel;

public class MainDataStatic {
    public static ConnectionDBModel appCDBModel;
    public static UsuarioModel usuarioSesionModel;
    public static MailerModel mailerModel;
    
    public static boolean isProcessing = false;
    
    static {
        MainDataStatic.appCDBModel = new ConnectionDBModel("127.0.0.1", "3306", "sigi", "root", "");
        MainDataStatic.usuarioSesionModel = null;
        MainDataStatic.mailerModel = new MailerModel("smtp.gmail.com", "465", "gramiren.ca.ca@gmail.com", "gramiren1.", true);
    }
    
    public static void setDataBase(final ConnectionDBModel appCDBModel) {
        MainDataStatic.appCDBModel = appCDBModel;
    }
    
    public static ConnectionDBModel getAppCDBModel() {
        return MainDataStatic.appCDBModel;
    }
    
    public static void setAppCDBModel(final ConnectionDBModel appCDBModel) {
        MainDataStatic.appCDBModel = appCDBModel;
    }
    
    public static UsuarioModel getUsuarioSesionModel() {
        return MainDataStatic.usuarioSesionModel;
    }
    
    public static void setUsuarioSesionModel(final UsuarioModel usuarioSesionModel) {
        MainDataStatic.usuarioSesionModel = usuarioSesionModel;
    }
    
    public static MailerModel getMailerModel() {
        return MainDataStatic.mailerModel;
    }
    
    public static void setMailerModel(final MailerModel mailerModel) {
        MainDataStatic.mailerModel = mailerModel;
    }
    
    public static void main(final String[] jeje) throws IOException {
        System.out.println("Opening notepad");
        final Runtime runTime = Runtime.getRuntime();
        final Process process = runTime.exec("notepad");
        process.destroy();
    }

	public static boolean isProcessing() {
		return isProcessing;
	}

	public static void setProcessing(boolean isProcessing) {
		MainDataStatic.isProcessing = isProcessing;
	}
}