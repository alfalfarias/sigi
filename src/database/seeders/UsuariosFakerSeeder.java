package database.seeders;

import java.sql.SQLException;

import models.UsuarioModel;
import services.UsuarioService;

public class UsuariosFakerSeeder {
	public static void main(String[] args) throws SQLException {
		Object[][] usuarios = new Object[][] {
			{"1253", "13.258.159", "Jos� Ant�nio", "Higuera Ferm�n", UsuarioModel.USUARIO_TIPOS[0], "jhiguera", UsuarioModel.USUARIO_NACIONALIDAD[0]}, 
			{"1254", "13.258.151", "Javier Eduardo", "Belmonte Pinto", UsuarioModel.USUARIO_TIPOS[2], "jbelmonte", UsuarioModel.USUARIO_NACIONALIDAD[1]}, 
			{"1255", "13.258.152", "Robert Andr�s", "Romero", UsuarioModel.USUARIO_TIPOS[3], "rromero", UsuarioModel.USUARIO_NACIONALIDAD[0]}, 
			{"1276", "13.258.153", "Mar�a Lourdes", "Aguilera", UsuarioModel.USUARIO_TIPOS[0], "maguilera", UsuarioModel.USUARIO_NACIONALIDAD[0]}, 
			{"1279", "13.258.154", "Mariana", "Rend�n", UsuarioModel.USUARIO_TIPOS[2], "mrendon", UsuarioModel.USUARIO_NACIONALIDAD[0]}, 
			{"1301", "13.258.155", "Estefan�a", "Fuentes", UsuarioModel.USUARIO_TIPOS[3], "efuentes", UsuarioModel.USUARIO_NACIONALIDAD[0]}, 
			{"1354", "13.258.156", "Adri�n Jos�", "Velasquez Soto", UsuarioModel.USUARIO_TIPOS[2], "avelasquez", UsuarioModel.USUARIO_NACIONALIDAD[1]}, 
			{"1355", "13.258.157", "Juan Miguel", "Salazar", UsuarioModel.USUARIO_TIPOS[3], "jsalazar", UsuarioModel.USUARIO_NACIONALIDAD[0]}, 
			{"1392", "13.258.158", "Ernesto Jes�s", "Prado", UsuarioModel.USUARIO_TIPOS[3], "eprado", UsuarioModel.USUARIO_NACIONALIDAD[0]}, 
		};
		for(Object[] usuario: usuarios) {
			UsuarioModel usuarioModel = new UsuarioModel(String.valueOf(usuario[2]), String.valueOf(usuario[3]), String.valueOf(usuario[6]), String.valueOf(usuario[1]), String.valueOf(usuario[5]+"@gmail.com"), String.valueOf(usuario[5]), String.valueOf(usuario[5]), String.valueOf(usuario[4]), null);
			usuarioModel = UsuarioService.create(usuarioModel);
			System.out.println("USUARIO #"+usuarioModel.getId()+" CREADO");
		}
		System.out.println("UsuarioFakerSeeder FINISHED");
	}
}
