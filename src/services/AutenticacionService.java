package services;

import java.sql.SQLException;
import java.util.Date;

import database.MainDataStatic;
import models.UsuarioModel;

public class AutenticacionService extends MySQL{
    public AutenticacionService() {
    	super();
    }
	public static boolean login(String username, String clave) throws SQLException {
		connect();
		
		String sql = "SELECT * FROM `usuarios` WHERE username = ? AND clave = ? LIMIT 1";
		
		preparedStatement = connection.prepareStatement(sql);
		statement = connection.createStatement();
		
		preparedStatement.setString(1, username);
		preparedStatement.setString(2, clave);
		resultSet = preparedStatement.executeQuery();
		UsuarioModel usuarioModel = null;
		if (resultSet.next()){
			usuarioModel = new UsuarioModel();
			usuarioModel.setId(resultSet.getLong("id"));
			usuarioModel.setNombres(resultSet.getString("nombres"));
			usuarioModel.setApellidos(resultSet.getString("apellidos"));
			usuarioModel.setNacionalidad(resultSet.getString("nacionalidad"));
			usuarioModel.setCedula(resultSet.getString("cedula"));
			usuarioModel.setEmail(resultSet.getString("email"));
			usuarioModel.setUsername(resultSet.getString("username"));
			usuarioModel.setClave(resultSet.getString("clave"));
			usuarioModel.setTipo(resultSet.getString("tipo"));
			usuarioModel.setFechaCreacion(resultSet.getDate("fecha_creacion"));	
			
			MainDataStatic.setUsuarioSesionModel(usuarioModel);

			System.out.format("%s, %s \n", usuarioModel.getId(), usuarioModel.getNombres());
		}
		disconnect();
		return usuarioModel != null;
	}
	
	public static void logout() {
		MainDataStatic.setUsuarioSesionModel(null);
	}
	
	public static UsuarioModel sendPasswordToEmail(final String email) throws SQLException {
        connect();
        final String sql = "SELECT * FROM `usuarios` WHERE email = ? LIMIT 1";
        AutenticacionService.preparedStatement = AutenticacionService.connection.prepareStatement(sql);
        AutenticacionService.statement = AutenticacionService.connection.createStatement();
        AutenticacionService.preparedStatement.setString(1, email);
        AutenticacionService.resultSet = AutenticacionService.preparedStatement.executeQuery();
        UsuarioModel usuarioModel = null;
        if (AutenticacionService.resultSet.next()) {
            usuarioModel = new UsuarioModel();
            usuarioModel.setId(AutenticacionService.resultSet.getLong("id"));
            usuarioModel.setNombres(AutenticacionService.resultSet.getString("nombres"));
            usuarioModel.setApellidos(AutenticacionService.resultSet.getString("apellidos"));
            usuarioModel.setNacionalidad(AutenticacionService.resultSet.getString("nacionalidad"));
            usuarioModel.setCedula(AutenticacionService.resultSet.getString("cedula"));
            usuarioModel.setEmail(AutenticacionService.resultSet.getString("email"));
            usuarioModel.setUsername(AutenticacionService.resultSet.getString("username"));
            usuarioModel.setClave(AutenticacionService.resultSet.getString("clave"));
            usuarioModel.setTipo(AutenticacionService.resultSet.getString("tipo"));
            usuarioModel.setFechaCreacion((Date)AutenticacionService.resultSet.getDate("fecha_creacion"));
            MainDataStatic.setUsuarioSesionModel(usuarioModel);
//            System.out.format("%s, %s \n", usuarioModel.getId(), usuarioModel.getNombres());
        }
        disconnect();
        return usuarioModel;
    }
	
	public static void main(String[] args)
	{
		try {
			AutenticacionService.login("Admi4nistrador", "Administrador");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
