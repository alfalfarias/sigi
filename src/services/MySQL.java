package services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.MainDataStatic;
import models.ConnectionDBModel;

public class MySQL {
	protected static ConnectionDBModel appCDBModel = MainDataStatic.appCDBModel;
    protected static Connection connection = null;
    protected static PreparedStatement preparedStatement = null;
    protected static Statement statement = null;
    protected static ResultSet resultSet = null;
	
	public static void connect() throws SQLException {
		connection = DriverManager.getConnection("jdbc:mysql://"+appCDBModel.getHost()+":"+appCDBModel.getPort()+"/" + appCDBModel.getDatabase(), appCDBModel.getUsername(), appCDBModel.getPassword());
	}
	public static void disconnect() throws SQLException{

		if (preparedStatement != null){
			preparedStatement.close();
		}
        if (statement != null){
            statement.close();
        }
        if (resultSet != null) {
        	resultSet.close();
        }
        if (connection != null) {
            connection.close();
        }
	}
}

