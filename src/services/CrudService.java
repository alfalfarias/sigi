package services;

import java.util.ArrayList;

public class CrudService<Model> {
	public ArrayList<Model> read() {
		ArrayList<Model> models = new ArrayList<Model>();
		return models;
	}

	public Model create(Model model) {
		return model;
	}
	
	public Model show(Model model) {
		return model;
	}
	
	public Model update(Model model) {
		return model;
	}
	
	public Model delete(Model model) {
		return model;
	}
}
