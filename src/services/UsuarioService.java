package services;

import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import models.UsuarioModel;

public class UsuarioService extends MySQL {
	public static UsuarioModel create(UsuarioModel usuarioModel) throws SQLException {
		connect();
		String sql = "INSERT INTO `usuarios` (`nombres`, `apellidos`, `nacionalidad`, `cedula`, `email`, `username`, `clave`, `tipo`, `fecha_creacion`) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP);";
		
		preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

		preparedStatement.setString(1, usuarioModel.getNombres());
		preparedStatement.setString(2, usuarioModel.getApellidos());
		preparedStatement.setString(3, usuarioModel.getNacionalidad());
		preparedStatement.setString(4, usuarioModel.getCedula());
		preparedStatement.setString(5, usuarioModel.getEmail());
		preparedStatement.setString(6, usuarioModel.getUsername());
		preparedStatement.setString(7, usuarioModel.getClave());
		preparedStatement.setString(8, usuarioModel.getTipo());
		
		if (preparedStatement.executeUpdate() != 0){
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()){
				usuarioModel.setId(resultSet.getLong(1));
			}
		}
		disconnect();
		
		return usuarioModel;
	}
	
	public static UsuarioModel update(UsuarioModel usuarioModel) throws SQLException {
		connect();
		String sql = "UPDATE `usuarios` SET `nombres` = ?, `apellidos` = ?, `nacionalidad` = ?, `cedula` = ?, `email` = ?, `username` = ?, `clave` = ?, `tipo` = ? WHERE `usuarios`.`id` = ?";
		
		preparedStatement = connection.prepareStatement(sql);

		preparedStatement.setString(1, usuarioModel.getNombres());
		preparedStatement.setString(2, usuarioModel.getApellidos());
		preparedStatement.setString(3, usuarioModel.getNacionalidad());
		preparedStatement.setString(4, usuarioModel.getCedula());
		preparedStatement.setString(5, usuarioModel.getEmail());
		preparedStatement.setString(6, usuarioModel.getUsername());
		preparedStatement.setString(7, usuarioModel.getClave());
		preparedStatement.setString(8, usuarioModel.getTipo());
		preparedStatement.setLong(9, usuarioModel.getId());
		preparedStatement.executeUpdate();

		disconnect();
		
		return usuarioModel;
	}

	public static void delete(Long id) throws SQLException{
		connect();
		String sql = "DELETE FROM `usuarios` WHERE `usuarios`.`id` = ?";
		
		preparedStatement = connection.prepareStatement(sql);
		preparedStatement.setLong(1, id);
		preparedStatement.executeUpdate();
		
		disconnect();
	}
	
	public static ArrayList<UsuarioModel> read(String where) throws SQLException {
		ArrayList<UsuarioModel> usuarios = new ArrayList<UsuarioModel>();
		
		connect();
		
		String sql = "SELECT * FROM `usuarios` "+where;
		
		preparedStatement = connection.prepareStatement(sql);
		statement = connection.createStatement();
		
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()){
			UsuarioModel usuarioModel = new UsuarioModel();
			usuarioModel.setId(resultSet.getLong("id"));
			usuarioModel.setNombres(resultSet.getString("nombres"));
			usuarioModel.setApellidos(resultSet.getString("apellidos"));
			usuarioModel.setNacionalidad(resultSet.getString("nacionalidad"));
			usuarioModel.setCedula(resultSet.getString("cedula"));
			usuarioModel.setEmail(resultSet.getString("email"));
			usuarioModel.setUsername(resultSet.getString("username"));
			usuarioModel.setClave(resultSet.getString("clave"));
			usuarioModel.setTipo(resultSet.getString("tipo"));
			usuarioModel.setFechaCreacion(resultSet.getDate("fecha_creacion"));			
			usuarios.add(usuarioModel);
//			System.out.format("%s, %s \n", usuarioModel.getId(), usuarioModel.getNombres());
		}
		disconnect();
		
		return usuarios;
	}
	
	public static UsuarioModel find(String where) throws SQLException {
		
		connect();
		
		String sql = "SELECT * FROM `usuarios` "+where;
		
		preparedStatement = connection.prepareStatement(sql);
		
		resultSet = preparedStatement.executeQuery();
		
		UsuarioModel usuarioModel = null;
		if (resultSet.next()) {
			usuarioModel = new UsuarioModel();
			usuarioModel.setId(resultSet.getLong("id"));
			usuarioModel.setNombres(resultSet.getString("nombres"));
			usuarioModel.setApellidos(resultSet.getString("apellidos"));
			usuarioModel.setNacionalidad(resultSet.getString("nacionalidad"));
			usuarioModel.setCedula(resultSet.getString("cedula"));
			usuarioModel.setEmail(resultSet.getString("email"));
			usuarioModel.setUsername(resultSet.getString("username"));
			usuarioModel.setClave(resultSet.getString("clave"));
			usuarioModel.setTipo(resultSet.getString("tipo"));
			usuarioModel.setFechaCreacion(resultSet.getDate("fecha_creacion"));			
//			System.out.format("%s, %s \n", usuarioModel.getId(), usuarioModel.getNombres());
		}
		disconnect();
		
		return usuarioModel;
	}

	public static void main(String[] args)
	{
		try {
			UsuarioService.find("");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
