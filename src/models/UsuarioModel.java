package models;

import java.util.Date;

public class UsuarioModel {
	
	public UsuarioModel() {}
	
	public UsuarioModel(String nombres, String apellidos, String nacionalidad, String cedula, String email,
				String username, String clave, String tipo, Date fecha_creacion) {
		super();
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.nacionalidad = nacionalidad;
		this.cedula = cedula;
		this.email = email;
		this.username = username;
		this.clave = clave;
		this.tipo = tipo;
		this.fecha_creacion = fecha_creacion;
	}
	
	public static String[] USUARIO_TIPOS = new String[] {"Administrador", "Gerente general", "Gerente de operaciones", "Gerente de proyectos"};
	public static String[] USUARIO_NACIONALIDAD = new String[] {"V", "E",};	
	
	Long id;
	String nombres;
	String apellidos;
	String nacionalidad;
	String cedula;
	String email;
	String username;
	String clave;
	String tipo;
	Date fecha_creacion;
	
	public static String[] getUSUARIO_TIPOS() {
		return USUARIO_TIPOS;
	}
	public static void setUSUARIO_TIPOS(String[] uSUARIO_TIPOS) {
		USUARIO_TIPOS = uSUARIO_TIPOS;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Date getFechaCreacion() {
		return fecha_creacion;
	}
	public void setFechaCreacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}
}
