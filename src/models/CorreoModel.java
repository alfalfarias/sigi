package models;

public class CorreoModel {
	String email;
	String subject;
	String text;
	String file;
	
	public CorreoModel() {
		
	}
	public CorreoModel(String email, String subject, String text, String file) {
		this.email = email;
		this.subject = subject;
		this.text = text;
		this.file = file;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
}
