package models;

public class MailerModel {
    String driver;
    String host;
    String port;
    String username;
    String password;
    boolean encryption;

    public MailerModel() { }
    
    public MailerModel(final String host, final String port, final String username, final String password, final boolean encryption) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.encryption = encryption;
    }
    
    public String getDriver() {
        return this.driver;
    }
    
    public void setDriver(final String driver) {
        this.driver = driver;
    }
    
    public String getHost() {
        return this.host;
    }
    
    public void setHost(final String host) {
        this.host = host;
    }
    
    public String getPort() {
        return this.port;
    }
    
    public void setPort(final String port) {
        this.port = port;
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(final String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(final String password) {
        this.password = password;
    }
    
    public boolean getEncryption() {
        return this.encryption;
    }
    
    public void setEncryption(final boolean encryption) {
        this.encryption = encryption;
    }

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MailerModel other = (MailerModel) obj;
		if (encryption != other.encryption)
			return false;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (port == null) {
			if (other.port != null)
				return false;
		} else if (!port.equals(other.port))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
}