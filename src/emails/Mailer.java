package emails;

import com.sun.mail.smtp.SMTPTransport;

import database.MainDataStatic;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public abstract class Mailer {

    // for example, smtp.mailgun.org
	protected static String SMTP_SERVER = MainDataStatic.mailerModel.getHost();
    protected static String USERNAME = MainDataStatic.mailerModel.getUsername();
    protected static String PASSWORD = MainDataStatic.mailerModel.getPassword();

    protected static String EMAIL_FROM = "info@gramiren.com";
    protected static String EMAIL_TO = "mariaaguilera3008@gmail.com, alfalfarias@gmail.com";
//    @SuppressWarnings("unused")
//    protected static String EMAIL_TO_CC = "";

    protected static String EMAIL_SUBJECT = "SIGI: Olvido de contrase�a";
    protected static String EMAIL_TEXT = "<h1>Sistema Integral de Gesti�n de Indicadores</h1> <p>Estimado usuario, su contrase�a de acceso al sistema es: ABC123</p>";
    protected static String EMAIL_FILE_PATH = null;
    
    public abstract void setData();
    
    public void send(String email) throws AddressException, MessagingException {
    	this.setData();
    	EMAIL_TO = email;
        Properties properties = System.getProperties();
		properties.put("mail.smtp.host", MainDataStatic.mailerModel.getHost());
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.port", MainDataStatic.mailerModel.getPort());
		properties.put("mail.smtp.auth", MainDataStatic.mailerModel.getEncryption());
		properties.put("mail.user", MainDataStatic.mailerModel.getUsername());
		properties.put("mail.password", MainDataStatic.mailerModel.getPassword());

		properties.put("mail.smtp.starttls.enable","true");
		properties.put("mail.smtp.debug", "true");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.socketFactory.port", 465);
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.socketFactory.fallback", "false");

        Session session = Session.getInstance(properties, null);
		session.setDebug(true);
        Message message = new MimeMessage(session);


        message.setFrom(new InternetAddress(EMAIL_FROM));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(EMAIL_TO, false));
        message.setSubject(EMAIL_SUBJECT);
		
		// TEXT email
//        message.setText(EMAIL_TEXT);

		// HTML email
        message.setDataHandler(new DataHandler(new HTMLDataSource(EMAIL_TEXT)));
        
     // Part two is attachment
        if (EMAIL_FILE_PATH != null) {
        	File file = new File(EMAIL_FILE_PATH);
        	if (file.exists()) {
                MimeBodyPart messageBodyPart = new MimeBodyPart();
            	// Create a multipar message
                Multipart multipart = new MimeMultipart();
                // Set text message part
                multipart.addBodyPart(messageBodyPart);
                DataSource source = new FileDataSource(file.getAbsolutePath());
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(file.getName());
                multipart.addBodyPart(messageBodyPart);
                message.setContent(multipart);
        	}
        }

		SMTPTransport transport = (SMTPTransport) session.getTransport("smtp");
		
		// connect
		transport.connect(SMTP_SERVER, USERNAME, PASSWORD);
		
		// send
		transport.sendMessage(message, message.getAllRecipients());

        System.out.println("Response: " + transport.getLastServerResponse());

        transport.close();
    }

    static class HTMLDataSource implements DataSource {

        private String html;

        public HTMLDataSource(String htmlString) {
            html = htmlString;
        }

        public InputStream getInputStream() throws IOException {
            if (html == null) throw new IOException("html message is null!");
            return new ByteArrayInputStream(html.getBytes());
        }

        public OutputStream getOutputStream() throws IOException {
            throw new IOException("This DataHandler cannot write HTML");
        }

        public String getContentType() {
            return "text/html";
        }

        public String getName() {
            return "HTMLDataSource";
        }
    }
}
