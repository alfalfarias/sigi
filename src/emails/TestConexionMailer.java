package emails;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.sun.mail.smtp.SMTPTransport;

import models.MailerModel;

public class TestConexionMailer extends Mailer {
	MailerModel mailerModel;
	
	public TestConexionMailer(MailerModel mailerModel) {
		this.mailerModel = mailerModel;
	}
	
	public void setData() {
	    EMAIL_FROM = "info@gramiren.com";
	    EMAIL_TO = "";

	    EMAIL_SUBJECT = "SIGI: Cambio de servidor SMTP";
	    EMAIL_TEXT = "<h1>Sistema Integral de Gesti�n de Indicadores</h1>"
	    		+ "<p>Estimado usuario, se han cambiado las credenciales de acceso "
	    		+ "al servidor de correos SMTP a esta nueva cuenta. "
	    		+ "Si recibi� este correo, el cambio fue realizado exitosamente.</p>";
	}
	
	public void send(String email) throws AddressException, MessagingException {
System.out.println("HEHEHEHEHEHEH");
		this.setData();
    	EMAIL_TO = email;
        Properties properties = System.getProperties();
		properties.put("mail.smtp.host", this.mailerModel.getHost());
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.port", this.mailerModel.getPort());
		properties.put("mail.smtp.auth", this.mailerModel.getEncryption());
		properties.put("mail.user", this.mailerModel.getUsername());
		properties.put("mail.password", this.mailerModel.getPassword());

		properties.put("mail.smtp.starttls.enable","true");
		properties.put("mail.smtp.debug", "true");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.socketFactory.port", this.mailerModel.getPort());
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.socketFactory.fallback", "false");

        Session session = Session.getInstance(properties, null);
		session.setDebug(true);
        Message message = new MimeMessage(session);


        message.setFrom(new InternetAddress(EMAIL_FROM));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(EMAIL_TO, false));
        message.setSubject(EMAIL_SUBJECT);
		
		// TEXT email
//        message.setText(EMAIL_TEXT);

		// HTML email
        message.setDataHandler(new DataHandler(new HTMLDataSource(EMAIL_TEXT)));

		SMTPTransport transport = (SMTPTransport) session.getTransport("smtp");
		
		// connect
		transport.connect(mailerModel.getHost(), mailerModel.getUsername(), mailerModel.getPassword());
		
		// send
		transport.sendMessage(message, message.getAllRecipients());

        System.out.println("Response: " + transport.getLastServerResponse());

        transport.close();
    }
}