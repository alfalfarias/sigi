/**
 * 
 */
package emails;

import models.CorreoModel;

/**
 * @author Lul� Aguilera
 *
 */
public class CorreoPersonaziladoMailer extends Mailer {
	CorreoModel correoModel;
	public CorreoPersonaziladoMailer(CorreoModel correoModel) {
		this.correoModel = correoModel;
	}
	public void setData() {
	    EMAIL_FROM = "info@gramiren.com";
	    EMAIL_TO = this.correoModel.getEmail();

	    EMAIL_SUBJECT = "SIGI: "+this.correoModel.getSubject();
	    EMAIL_TEXT = "<h1>Sistema Integral de Gesti�n de Indicadores</h1>"
	    		+ " <p>"+this.correoModel.getText()+"</p>";
	    EMAIL_FILE_PATH = this.correoModel.getFile();
	}
}