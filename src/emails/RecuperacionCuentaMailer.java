package emails;

import models.UsuarioModel;

public class RecuperacionCuentaMailer extends Mailer {
	UsuarioModel usuarioModel;
	public RecuperacionCuentaMailer(UsuarioModel usuarioModel) {
		this.usuarioModel = usuarioModel;
	}
	public void setData() {
	    EMAIL_FROM = "info@gramiren.com";
	    EMAIL_TO = "";

	    EMAIL_SUBJECT = "SIGI: Olvido de Contrase�a";
	    EMAIL_TEXT = "<h1>Sistema Integral de Gesti�n de Indicadores</h1>"
	    		+ " <p>Estimado usuario "+usuarioModel.getNombres()+" "+usuarioModel.getApellidos()+" "+","
	    				+ " su contrase�a de acceso al sistema es: "+usuarioModel.getClave()+"</p>";
	}
}